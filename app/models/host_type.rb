class HostType < ApplicationRecord
  validates_presence_of :name, :description, :created_by, :created_dttm
end
