unless ENV['RAILS_ENV'] == 'production' 
  require 'fabrication'

  namespace :dev do
    desc "Seed development database, maintain consistency"
    task prime: :environment do

      HostType.delete_all

      Fabricate(:host_type)
      Fabricate(:baremetal)
      Fabricate(:hypervisor)
      Fabricate(:container)

      puts "Created #{HostType.count} records for the Host Type table."
    end
  end

end

