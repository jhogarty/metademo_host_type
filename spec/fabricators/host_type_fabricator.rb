Fabricator(:host_type) do
  name          "Virtual"
  description   "Use this host type for virtual machine type hosts."
  created_by    "MetaDemo::HostType"
  created_dttm  Time.now
  updated_by    nil
  updated_dttm  nil
end

Fabricator(:baremetal, from: :host_type) do
  name          "Baremetal"
  description   "Use this host type for baremetal type hosts"
end

Fabricator(:hypervisor, from: :host_type) do
  name          "Hypervisor"
  description   "Use this host type for hypervisors"
end

Fabricator(:container, from: :host_type) do
  name          "Container"
  description   "Use this host type for container type hosts"
end

