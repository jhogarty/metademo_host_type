# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HostType, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:created_by) }
    it { is_expected.to validate_presence_of(:created_dttm) }
  end

  describe 'created_by' do
    it 'should not save if nil' do
      host_type = HostType.new(name: 'Baremetal', description: 'A baremetal host',
                               created_dttm: Time.now)
      expect(host_type.save).to be false
    end
    it 'should not save if empty' do
      host_type = HostType.new(name: 'Baremetal', description: 'A baremetal host',
                               created_by: '', created_dttm: Time.now)
      expect(host_type.save).to be false
    end
  end

end
