# README

**NOTE**: Please keep in mind that we have split the original application across
several repos for this prototyping of using Micro Services.  You should only 
need to do the database setup information below for the first repo of the 
collection.  In our case, we are starting with the repo for Host Types.

Since we are using Postgresql as the database, you may need to create new accounts
in Postgresql for our application.  See the config/database.yml file for example
accounts, or create your own and update the config/database.yml file.

Example of how to use psql to create the needed accounts:

```shell
psql -U postgres -h localhost -p 5432

create database metademo_dev;
create user metademo_user_dev with encrypted password 'Supp0rt!';
grant all privileges on database metademo_dev to metademo_user_dev;
ALTER USER metademo_user_dev CREATEDB;
ALTER USER metademo_user_dev SUPERUSER;

create database metademo_test;
create user metademo_user_test with encrypted password 'Supp0rt!';
grant all privileges on database metademo_test to metademo_user_test;
ALTER USER metademo_user_test CREATEDB;
ALTER USER metademo_user_test SUPERUSER;

create database metademo_prod;
create user metademo_user_prod with encrypted password '[SUPERSECRETHERE]';
grant all privileges on database metademo_prod to metademo_user_prod;
ALTER USER metademo_user_prod CREATEDB;
```

*NOTE:* If the databases have already been created for you, assuming you have
valid accounts and passwords, then you should be able to skip the step below for
running `rake db:create` and jump to the migrate rake task!


Update config/database.yml for database configuration

Run commands:  

```shell
rake db:create
rake db:migrate

rails db:migrate RAILS_ENV=test
```


**EXAMPLE:**

```shell
➜  metademo_host_type git:(enh_build_out_host_type_model) ✗ rake db:create
Database 'metademo_dev' already exists
Database 'metademo_test' already exists
➜  metademo_host_type git:(enh_build_out_host_type_model) ✗ rake db:migrate
== 20190630225852 CreateHostTypes: migrating ==================================
-- create_table(:host_types)
   -> 0.0705s
-- add_index(:host_types, :name, {:unique=>true})
   -> 0.0137s
== 20190630225852 CreateHostTypes: migrated (0.0845s) =========================

➜  metademo_host_type git:(enh_build_out_host_type_model) ✗ 
```

You can do a quick manual test to ensure the database setup and migration from
above is working by doing:

```shell
➜  metademo_host_type git:(enh_build_out_host_type_model) ✗ rails console
Running via Spring preloader in process 11357
Loading development environment (Rails 5.2.3)
irb: warn: can't alias context from irb_context.
2.6.3 :001 > HostType.count
   (0.8ms)  SELECT COUNT(*) FROM "host_types"
 => 0 
2.6.3 :002 > 

2.6.3 :001 > ht = HostType.new
 => #<HostType id: nil, name: nil, description: nil, created_by: nil, created_dttm: nil, updated_by: nil, updated_dttm: nil> 
2.6.3 :002 > ht.name = 'Virtual'
 => "Virtual" 
2.6.3 :003 > ht.description = 'Use this host type for hosts of type virtual machine.'
 => "Use this host type for hosts of type virtual machine." 
2.6.3 :004 > ht.created_by = 'HoGiHung'
 => "HoGiHung" 
2.6.3 :005 > ht.created_dttm = DateTime.now
 => Sun, 30 Jun 2019 20:02:34 -0400 
2.6.3 :006 > ht.save
   (0.7ms)  BEGIN
  HostType Create (1.5ms)  INSERT INTO "host_types" ("name", "description", "created_by", "created_dttm") VALUES ($1, $2, $3, $4) RETURNING "id"  [["name", "Virtual"], ["description", "Use this host type for hosts of type virtual machine."], ["created_by", "HoGiHung"], ["created_dttm", "2019-07-01 00:02:34.978003"]]
   (2.2ms)  COMMIT
 => true 
2.6.3 :007 > HostType.count
   (1.1ms)  SELECT COUNT(*) FROM "host_types"
 => 1 
2.6.3 :008 > HostType.all
  HostType Load (0.7ms)  SELECT  "host_types".* FROM "host_types" LIMIT $1  [["LIMIT", 11]]
 => #<ActiveRecord::Relation [#<HostType id: 1, name: "Virtual", description: "Use this host type for hosts of type virtual machi...", created_by: "HoGiHung", created_dttm: "2019-07-01 00:02:34", updated_by: nil, updated_dttm: nil>]> 
2.6.3 :009 > 
```


## SUMMARY

This repo is part of a collection of related repos with the prefix of metademo\_
The purpose of this repo, and the related ones, is to assist me with the idea of
prototyping a demo application based off of an existing producution application 
which uses Sinatra/Ruby-Grape, and convert that appliation to one that uses Ruby
 on Rails, splitting out resources into their own micro-service (API).

Currently the list of related repos are:

  - [MetaDemo Host](https://gitlab.com/jhogarty/metademo_host)
  - [MetaDemo Host Attr](https://gitlab.com/jhogarty/metademo_host_attr)
  - [MetaDemo Host Iface](https://gitlab.com/jhogarty/metademo_host_iface)
  - [MetaDemo Host Mount](https://gitlab.com/jhogarty/metademo_host_mount)
  - [MetaDemo Host Role](https://gitlab.com/jhogarty/metademo_host_role)
  - [MetaDemo Host Type](https://gitlab.com/jhogarty/metademo_host_type)
  - [MetaDemo Datacenter](https://gitlab.com/jhogarty/metademo_datacenter)


## SPECS

For each resource of the application, we will identify the attributes/columns
needed in the database.  Each attribute should have a defined type.  For example:

  - hostname      => String
  - memory        => Number
  - updated\_dttm => DateTime

We also need to identify which attributes/columns are *required*.  We should 
ensure that we have the proper validations in place for these columns as well
as defining validations for say FQDN (fully qualified domain names.)

Beyond the model inspection we also need to identify what the expected endpoints
are for the application, with related *actions*.  For example, for the host 
resource we expect to be able to use GET, POST, PUT, (PATCH), and DELETE.  For
those endpoints we expect the response to be in JSON.  We need to identify what
that reponse will look like for both a single record and a collection of records.

    ```ruby
      primary_key :id
      String :name, :index=>true
      String :description
      String :created_by
      DateTime :created_dttm
      String :updated_by
      DateTime :updated_dttm

      # NOTE: we are replacing the rails 'timestamps' with our own columns so
      #       that we match with the pre-existing database structure.
      #
      # REQUIRED: all columns are required
    ```

**REQUIRED ENDPOINTS**

  - GET    /ap1/v1/host\_types      # index
  - POST   /api/v1/host\_types      # create
  - GET    /ap1/v1/host\_types/:id  # show
  - PUT    /api/v1/host\_types/:id  # update
  - PATCH  /api/v1/host\_types/:id  # update
  - DELETE /api/v1/host\_types/:id  # delete

